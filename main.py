# Não altere a linha seguinte
import time

# Altere a linha seguinte adicionando zeros conforme o video do enunciado
MAX_RANGE = 10000000

def exercise_1(max_range):
    list_of_numbers = [num for num in range(max_range)]
    return list_of_numbers


def exercise_1_yield(max_range):
    numbers = 0
    while numbers < max_range:
        yield numbers
        numbers += 1
    return numbers




# Nao altere a partir dessa linha para baixo !!

# Sem YIELD
start = time.process_time()

exercise_1(MAX_RANGE)

end = time.process_time() - start
print(f"-- Tempo de execução SEM YIELD: {end:.6f} segundos --")


# Com YIELD
start = time.process_time()

exercise_1_yield(MAX_RANGE)

end = time.process_time() - start
print(f"-- Tempo de execução COM YIELD: {end:.6f} segundos --")